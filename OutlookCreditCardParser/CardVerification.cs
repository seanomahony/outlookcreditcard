﻿using System.Linq;
using System.Text.RegularExpressions;

namespace OutlookCreditCardParser
{
    public static class CardVerification
    {
        public static CardType IsValid(string cardNum)
        {
            CardType type = CardType.Invalid;
            string numFormatted = Regex.Replace(cardNum, "[^A-Za-z0-9]", "");
            if (numFormatted.Length >= 13 && numFormatted.Length <= 16)
            {
                //Luhn algorithm is a checksum formula to verify validity of credit card numbers
                if (LuhnCheck(numFormatted))
                {
                    switch (numFormatted.Substring(0, 1))
                    {
                        case "0":
                            break;
                        case "3":
                            if(numFormatted.Substring(1, 1) == "4" || numFormatted.Substring(1, 1) == "7")
                            {
                                type = CardType.AmericanExpress;
                            }
                            break;
                        case "4":
                            type = CardType.Visa; 
                            break;
                        case "5":
                            type = CardType.MasterCard;
                            break;
                        default:
                            break;
                    }
                }
            }
            return type;
        }

        private static bool LuhnCheck(string cardNumber)
        {
            return cardNumber.All(char.IsDigit) && cardNumber.Reverse()
            .Select(c => c - '0')
            .Select((thisNum, i) => i % 2 == 0
                ? thisNum
                : ((thisNum *= 2) > 9 ? thisNum - 9 : thisNum)
            ).Sum() % 10 == 0;
        }
    }
}

public enum CardType
{
    Invalid = 0,
    AmericanExpress = 3,
    Visa = 4,
    MasterCard = 5
}
