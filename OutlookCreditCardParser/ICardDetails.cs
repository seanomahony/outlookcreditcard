﻿namespace OutlookCreditCardParser
{
    public interface ICardDetails
    {
        string CardNumber { get; }
        CardType Issuer { get; }
    }
}
