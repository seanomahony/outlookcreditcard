﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OutlookCreditCardParser;

namespace UnitTestProject
{
    [TestClass]
    public class CardVerificationTest
    {
        [TestMethod]
        public void EmptyStringReturnsInvalid()
        {
            var result = CardVerification.IsValid("");

            Assert.AreEqual(CardType.Invalid, result);
        }

        [TestMethod]
        public void NumberReturnsVisa()
        {
            var result = CardVerification.IsValid("4916293385903683");

            Assert.AreEqual(CardType.Visa, result);
        }

        [TestMethod]
        public void NumberReturnsMasterCard()
        {
            var result = CardVerification.IsValid("5241162167849369");

            Assert.AreEqual(CardType.MasterCard, result);
        }

        [TestMethod]
        public void NumberReturnsAmex()
        {
            var result = CardVerification.IsValid("374492926417004");

            Assert.AreEqual(CardType.AmericanExpress, result);
        }

        [TestMethod]
        public void NumberWithSpacesReturnsVisa()
        {
            var result = CardVerification.IsValid("4916 2933 8590 3683");

            Assert.AreEqual(CardType.Visa, result);
        }

        [TestMethod]
        public void NumberWithHyphensReturnsVisa()
        {
            var result = CardVerification.IsValid("4916-2933-8590-3683");

            Assert.AreEqual(CardType.Visa, result);
        }

        [TestMethod]
        public void InvalidNumberReturnsInvalid()
        {
            var result = CardVerification.IsValid("491629338590368");

            Assert.AreEqual(CardType.Invalid, result);
        }
    }
}
