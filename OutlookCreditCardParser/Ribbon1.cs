﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Office.Tools.Ribbon;
using Outlook = Microsoft.Office.Interop.Outlook;
using System.Windows.Forms;
using System.Text.RegularExpressions;

namespace OutlookCreditCardParser
{
    public partial class Ribbon1
    {
        private void Ribbon1_Load(object sender, RibbonUIEventArgs e)
        {

        }

        private void ParseMailButton_Click(object sender, RibbonControlEventArgs e)
        {
            List<ICardDetails> cardsFound = new List<ICardDetails>();
            // Get Application application
            Outlook.Application application = Globals.ThisAddIn.Application;

            // Get the current item for this Inspecto object and check if is type
            // of MailItem
            Outlook.Inspector inspector = application.ActiveInspector();
            if (inspector?.CurrentItem is Outlook.MailItem mailItem)
            {
                //create array of strings split by alpha characters
                string[] numbers = Regex.Split(mailItem.Body, "\\D+");
                numbers = numbers.Where(x => !string.IsNullOrEmpty(x)).ToArray();

                foreach (string num in numbers)
                {
                    switch (CardVerification.IsValid(num))
                    {
                        case CardType.Visa:
                            cardsFound.Add(new VisaCard(num));
                            break;
                        case CardType.AmericanExpress:
                            cardsFound.Add(new AmericanExpress(num));
                            break;
                        case CardType.MasterCard:
                            cardsFound.Add(new MasterCard(num));
                            break;
                    }
                }

                if(cardsFound.Count > 0)
                {
                    CSV.WriteMailToCSV(mailItem);
                    MessageBox.Show("Credit card detected. Mail added to CSV at " + CSV.retFilePath);
                }
                else
                {
                    MessageBox.Show("No credit card detected.");
                }
            }
        }        
    }
}
