﻿namespace OutlookCreditCardParser
{
    public class VisaCard : ICardDetails
    {
        string ICardDetails.CardNumber { get { return _cardNumber; } }
        CardType ICardDetails.Issuer { get { return _issuer; } }

        private readonly CardType _issuer;
        private readonly string _cardNumber;

        public VisaCard(string number)
        {
            _cardNumber = number;
            _issuer = CardType.Visa;
            //institution = int.Parse(number.Substring(1, 5));
            //personalId = int.Parse(cardNumber.Substring(6, 9));
        }        
    }

    public class MasterCard : ICardDetails
    {
        string ICardDetails.CardNumber { get { return _cardNumber; } }
        CardType ICardDetails.Issuer { get { return _issuer; } }

        private readonly CardType _issuer;
        private readonly string _cardNumber;

        public MasterCard(string number)
        {
            _cardNumber = number;
            _issuer = CardType.MasterCard;
            //institution = int.Parse(number.Substring(1, 5));
            //personalId = int.Parse(cardNumber.Substring(6, 9));
        }
    }

    public class AmericanExpress : ICardDetails
    {
        string ICardDetails.CardNumber { get { return _cardNumber; } }
        CardType ICardDetails.Issuer { get { return _issuer; } }

        private readonly CardType _issuer;
        private readonly string _cardNumber;

        public AmericanExpress(string number)
        {
            _cardNumber = number;
            _issuer = CardType.AmericanExpress;
            //institution = int.Parse(number.Substring(1, 5));
            //personalId = int.Parse(cardNumber.Substring(6, 9));
        }
    }
}
