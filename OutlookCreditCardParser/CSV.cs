﻿using CsvHelper;
using System;
using System.IO;
using System.Text.RegularExpressions;
using Outlook = Microsoft.Office.Interop.Outlook;

namespace OutlookCreditCardParser
{
    public static class CSV
    {
        // search the file below the current directory
        public static readonly string retFilePath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\OutlookCreditCardParser\\ReceivedEmails.csv";
        static readonly string archiveFilePath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\OutlookCreditCardParser\\archReceivedEmails.csv";
        static readonly string retFolderPath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\OutlookCreditCardParser";

        /// Check the CSV file in application directory. If it is not available then create it
        private static void CheckCSVFilePath()
        {
            //create a text file
            if (!File.Exists(retFilePath) == true)
            {
                CreateFile();
            }
        }

        private static void CreateFile()
        {
            Directory.CreateDirectory(retFolderPath);
            FileStream fs = new FileStream(retFilePath, FileMode.OpenOrCreate, FileAccess.ReadWrite);
            fs.Close();

            //add headers to file
            using (StreamWriter sw = new StreamWriter(retFilePath, true))
            using (CsvWriter cw = new CsvWriter(sw))
            {
                cw.Configuration.Delimiter = ",";
                cw.WriteHeader<MailContent>();
                cw.NextRecord();
            }
        }

        public static void WriteMailToCSV(Outlook.MailItem mailItem)
        {
            try
            {
                //Create file if not done so already
                CheckCSVFilePath();

                //check file size and backup if necessary
                CheckCSVFileSize();

                //remove all punctuation for comma delimiter
                MailContent mailContent = new MailContent
                {
                    DateReceived = mailItem.ReceivedTime.ToString(),
                    SenderName = Regex.Replace(mailItem.SenderName ?? String.Empty, @"[^\w\s]", "<>"),
                    SenderAddress = mailItem.SenderEmailAddress,
                    Subject = Regex.Replace(mailItem.Subject ?? String.Empty, @"[^\w\s]", "<>"),
                    Body = Regex.Replace(mailItem.Body ?? String.Empty, @"[^\w\s]", "<>"),
                };

                mailContent.Subject = Regex.Replace(mailContent.Subject, @"\\r\\n", " <> ");
                mailContent.Body = Regex.Replace(mailContent.Body, @"\r\n", " <> ");

                using (StreamWriter sw = new StreamWriter(retFilePath, true))
                using (CsvWriter cw = new CsvWriter(sw))
                {
                    cw.Configuration.Delimiter = ",";
                    cw.WriteRecord<MailContent>(mailContent);
                    cw.NextRecord();
                }
            }
            catch(Exception ex)
            {
                Logging.ErrorRoutine(true, ex);
            }
        }

        private static void CheckCSVFileSize()
        {
            //if file is greater than 10Mb, archive file and create new
            //old archive file is overwritten
            FileInfo fileInfo = new FileInfo(retFilePath);
            if (fileInfo.Length > 10000000)
            {
                File.Delete(archiveFilePath);
                File.Move(retFilePath, archiveFilePath);
                CreateFile();
            }
        }
    }

    internal class MailContent
    {
        public string DateReceived { get; set; }
        public string SenderName { get; set; }
        public string SenderAddress { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
    }
}
