# OutlookCreditCard

Outlook VSTO add-in to parse emails for credit cards

Add-in detects Visa, Mastercard and AMEX card numbers in the correct formats.
If a credit card number is detected then the content of the email is added to a CSV file located 
at \OutlookCreditCardParser\ReceivedEmails.csv in My Documents.

Punctuations and line breaks are replaced with <> to avoid formatting issues with the CSV.

To run the add-in open the mail by double-clicking and go to the Add-Ins tab in the new window. 
A messagebox will appear giving the result of the parsing.

The following site contains sample credit card numbers used for testing:
https://www.freeformatter.com/credit-card-number-generator-validator.html

This project uses the CSVHelper library.